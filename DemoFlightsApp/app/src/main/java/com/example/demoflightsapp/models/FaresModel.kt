package com.example.demoflightsapp.models

class FaresModel(
    var providerId: Int,
    var fare: Int,

    // Temp vars
    @Transient var providerName: String?
)