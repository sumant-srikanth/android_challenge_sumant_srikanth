package com.example.demoflightsapp

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demoflightsapp.interfaces.DataCallback
import com.example.demoflightsapp.models.FlightsModel
import com.example.demoflightsapp.models.FlightsResponse
import com.example.demoflightsapp.repos.FlightsRepo
import com.example.imagegrid.api.APIClient
import kotlin.Comparator
import kotlin.collections.ArrayList

class MainViewModel() : ViewModel() {

    val sortedFlights: MediatorLiveData<ArrayList<FlightsModel>> = MediatorLiveData<ArrayList<FlightsModel>>()

    private val flights: MutableLiveData<ArrayList<FlightsModel>> = MutableLiveData<ArrayList<FlightsModel>>()
    val sortType: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = 0 }

    companion object {
        public val SORT_DEFAULT = 0
        public val SORT_FARE = 1
        public val SORT_DEPARTURE = 2
        public val SORT_ARRIVAL = 3
    }

    private val repo: FlightsRepo

    init {
        repo = FlightsRepo(APIClient())

        sortedFlights.addSource(sortType, {
            sortData()
        })
        sortedFlights.addSource(flights, {
            sortData()
        })
    }

    inner class FareComparator : Comparator<FlightsModel> {
        override fun compare(o1: FlightsModel, o2: FlightsModel): Int {
            val minFare1 = o1.fares.map { it.fare }.min()!!
            val minFare2 = o2.fares.map { it.fare }.min()!!
            return minFare1 - minFare2
        }
    }

    inner class DepartureComparator : Comparator<FlightsModel> {
        override fun compare(o1: FlightsModel, o2: FlightsModel): Int {
            return (o1.departureTime - o2.departureTime).toInt()
        }
    }

    inner class ArrivalComparator : Comparator<FlightsModel> {
        override fun compare(o1: FlightsModel, o2: FlightsModel): Int {
            return (o1.arrivalTime - o2.arrivalTime).toInt()
        }
    }

    fun fetchData() {
        repo.fetchFlights(object : DataCallback<FlightsResponse> {
            override fun onSuccess(response: FlightsResponse) {
                val airlinesMap = response.appendix.airlines
                val providersMap = response.appendix.providers
                val airportsMap = response.appendix.airports

                response.flights.forEach { flight ->
                    flight.airlineName = airlinesMap[flight.airlineCode]
                    flight.originName = airportsMap[flight.originCode]
                    flight.destinationName = airportsMap[flight.destinationCode]
                    for (fare in flight.fares) {
                        fare.providerName = providersMap[fare.providerId]
                    }
                }

                flights.value = response.flights
            }

            override fun onFailure(t: Throwable, isCancelled: Boolean) {

            }

        })
    }

    private fun sortData() {
        flights.value ?: return

        val newList: ArrayList<FlightsModel> = ArrayList(flights.value)
        if (sortType.value == 1)
            newList.sortWith(FareComparator())
        else if (sortType.value == 2)
            newList.sortWith(DepartureComparator())
        else if (sortType.value == 3)
            newList.sortWith(ArrivalComparator())

        sortedFlights.value = newList
    }

}