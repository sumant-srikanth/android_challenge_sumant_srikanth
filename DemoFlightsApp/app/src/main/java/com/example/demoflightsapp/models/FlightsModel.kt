package com.example.demoflightsapp.models

class FlightsModel(
    var originCode: String,
    var destinationCode: String,
    var departureTime: Long,
    var arrivalTime: Long,
    var fares: ArrayList<FaresModel>,
    var airlineCode: String,
    var `class`: String,

    // Temp vars
    @Transient var originName: String?,
    @Transient var destinationName: String?,
    @Transient var airlineName: String?
)