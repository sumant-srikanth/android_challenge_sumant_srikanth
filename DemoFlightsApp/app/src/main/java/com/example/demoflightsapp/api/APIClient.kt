package com.example.imagegrid.api

import com.example.demoflightsapp.Util
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.ResponseBody
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class APIClient {

    companion object {

        @JvmStatic
        fun getClient(baseUrl: String, timeout: Int = 20000): Retrofit? {
            try {
                val client = OkHttpClient.Builder().addInterceptor(CustomInterceptor())
                    .writeTimeout(timeout.toLong(), TimeUnit.MILLISECONDS)
                    .readTimeout(timeout.toLong(), TimeUnit.MILLISECONDS)
                    .connectTimeout(timeout.toLong(), TimeUnit.MILLISECONDS)
                    .retryOnConnectionFailure(true)
                    .build()

                return Retrofit.Builder().baseUrl(baseUrl).client(client).addConverterFactory(buildGsonConverter())
                    .build()

            } catch (t: Exception) {
                Util.writeDebugLog(t)
            }

            return null
        }

        class CustomInterceptor : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                var request = chain.request()

                // ADD CUSTOM HEADERS
                request = request.newBuilder()
                    .method(request.method(), request.body())
                    .build()

                Util.writeDebugLog(bodyToString(request), "HTTP REQUEST")

                var response: okhttp3.Response = chain.proceed(request)

                Util.writeDebugLog("RESPONSE = " + response.code() + " " + response.message(), "HTTP RESPONSE")

                if (response.body() != null) {
                    var rawJson = ""
                    try {
                        rawJson = response.body()!!.string()
                        Util.writeDebugLog(rawJson, "HTTP RESPONSE")
                    } catch (t: Exception) {
                        Util.writeDebugLog(t)
                    }

                    // Rebuild response because it can only be used once
                    response = response.newBuilder()
                        .body(ResponseBody.create(response.body()!!.contentType(), rawJson)).build()
                }

                return response
            }
        }

        private fun bodyToString(request: Request): String {
            try {
                val copy = request.newBuilder().build()
                var requestStr = copy.method() + " => " + copy.url().toString()
                if (copy.body() != null) {
                    val buffer = Buffer()
                    copy.body()!!.writeTo(buffer)
                    val bufferStr = buffer.readUtf8()
                    if (!bufferStr.isEmpty())
                        requestStr += " : $bufferStr"
                }
                return requestStr
            } catch (t: Exception) {
                Util.writeDebugLog(t)
                return ""
            }

        }

        private fun buildGsonConverter(): GsonConverterFactory {
            val gsonBuilder = GsonBuilder()
            val myGson = gsonBuilder.create()

            return GsonConverterFactory.create(myGson)
        }
    }
}