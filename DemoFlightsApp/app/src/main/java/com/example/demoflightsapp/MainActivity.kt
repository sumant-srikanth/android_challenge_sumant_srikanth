package com.example.demoflightsapp

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demoflightsapp.adapters.FlightsAdapter
import com.example.demoflightsapp.models.FlightsModel

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    lateinit var vm: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val adapter = FlightsAdapter()

        rvFlights.adapter = adapter
        rvFlights.layoutManager = LinearLayoutManager(this)

        vm = ViewModelProviders.of(this).get(MainViewModel::class.java) as MainViewModel
        vm.sortedFlights.observe(this, object : Observer<ArrayList<FlightsModel>> {
            override fun onChanged(t: ArrayList<FlightsModel>) {
                adapter.setData(t)
            }
        })

        vm.fetchData()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_sort_default -> {
                vm.sortType.value = MainViewModel.SORT_DEFAULT
                return true
            }
            R.id.action_sort_fare -> {
                vm.sortType.value = MainViewModel.SORT_FARE
                return true
            }
            R.id.action_sort_dep -> {
                vm.sortType.value = MainViewModel.SORT_DEPARTURE
                return true
            }
            R.id.action_sort_arr -> {
                vm.sortType.value = MainViewModel.SORT_ARRIVAL
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
