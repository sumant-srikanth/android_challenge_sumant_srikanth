package com.example.demoflightsapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.demoflightsapp.R
import com.example.demoflightsapp.models.FaresModel
import java.text.SimpleDateFormat
import java.util.*


class FaresAdapter(val data: ArrayList<FaresModel>) : RecyclerView.Adapter<FaresAdapter.ViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (!::context.isInitialized)
            context = parent.context

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_fares, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]

        holder.provider.text = item.providerName
        holder.fare.text = "₹" + item.fare
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val provider: TextView = itemView.findViewById(R.id.provider)
        val fare: TextView = itemView.findViewById(R.id.fare)
    }

}