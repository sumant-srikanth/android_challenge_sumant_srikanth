package com.example.demoflightsapp.repos

import com.example.demoflightsapp.Util
import com.example.demoflightsapp.interfaces.DataCallback
import com.example.demoflightsapp.models.FlightsResponse
import com.example.imagegrid.api.APIClient
import com.example.imagegrid.api.APIInterface

class FlightsRepo(val apiClient: APIClient, val timeout: Int = 20000) {

    private var client: APIInterface? = null

    init {
        client = APIClient.getClient(
            Util.BASE_URL,
            timeout
        )?.create(APIInterface::class.java)
    }

    fun fetchFlights(callback: DataCallback<FlightsResponse>) {
        client?.getFlights()?.enqueue(DataCallback.convertToRetrofit(callback))
    }
}