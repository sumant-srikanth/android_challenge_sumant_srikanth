package com.example.demoflightsapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demoflightsapp.R
import com.example.demoflightsapp.Util
import com.example.demoflightsapp.models.FlightsModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FlightsAdapter : RecyclerView.Adapter<FlightsAdapter.ViewHolder>() {
    private lateinit var context: Context
    private val data: ArrayList<FlightsModel> = ArrayList()

    private val timeFormat: SimpleDateFormat = SimpleDateFormat("HH:mm", Locale.US)
    private val dateFormat: SimpleDateFormat = SimpleDateFormat("ddd, dd MMM", Locale.US)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (!::context.isInitialized)
            context = parent.context

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_flights, parent, false))
    }

    fun setData(newData: List<FlightsModel>) {
        data.clear()
        data.addAll(newData)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]

        holder.origin.text = item.originName
        holder.dest.text = item.destinationName

        holder.airlineName.text = item.airlineName
        holder.className.text = item.`class`

        holder.duration.text = "${Util.getHoursMinsStringFromMillis((item.arrivalTime - item.departureTime))}"

        holder.origin1.text =
            "${item.originCode} ${Util.getDateStringFromMillis(item.departureTime, timeFormat)}"
        holder.depDate.text =
            "${Util.getDateStringFromMillis(item.departureTime, dateFormat)}"


        holder.dest1.text =
            "${item.destinationCode} ${Util.getDateStringFromMillis(item.arrivalTime, timeFormat)}"
        holder.arrivalDate.text =
            "${Util.getDateStringFromMillis(item.arrivalTime, dateFormat)}"

        holder.rvFares.adapter = FaresAdapter(item.fares)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val origin: TextView = itemView.findViewById(R.id.origin)
        val dest: TextView = itemView.findViewById(R.id.dest)

        val airlineName: TextView = itemView.findViewById(R.id.airlineName)

        val origin1: TextView = itemView.findViewById(R.id.origin1)
        val depDate: TextView = itemView.findViewById(R.id.departureDate)

        val dest1: TextView = itemView.findViewById(R.id.dest1)
        val arrivalDate: TextView = itemView.findViewById(R.id.arrivalDate)

        val className: TextView = itemView.findViewById(R.id.className)
        val duration: TextView = itemView.findViewById(R.id.duration)

        val rvFares: RecyclerView = itemView.findViewById(R.id.rvFares)

        init {
            rvFares.layoutManager = LinearLayoutManager(itemView.context)
            rvFares.isNestedScrollingEnabled = false
        }

    }

}