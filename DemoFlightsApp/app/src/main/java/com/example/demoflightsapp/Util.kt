package com.example.demoflightsapp

import java.text.SimpleDateFormat
import java.util.*

class Util {

    companion object {
        public const val BASE_URL = "http://www.mocky.io"

        public fun writeDebugLog(e: Throwable) {
            e.printStackTrace()
        }

        public fun writeDebugLog(s: String, tag: String? = "") {
            println("$tag >>>>>> $s")
        }

        public fun getHoursMinsStringFromMillis(millis: Long): String {
            val seconds = millis / 1000
            //val s = seconds % 60
            val m = seconds / 60 % 60
            val h = seconds / (60 * 60) % 24
            return String.format("%dh %02dm", h, m)
        }

        public fun getDateStringFromMillis(millis: Long, toFormat: SimpleDateFormat): String? {
            try {
                val d = Date(millis)
                return toFormat.format(d)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return ""
        }
    }
}