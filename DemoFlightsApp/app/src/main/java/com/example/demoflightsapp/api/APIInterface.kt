package com.example.imagegrid.api

import com.example.demoflightsapp.models.FlightsResponse
import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {

    @GET("/v2/5979c6731100001e039edcb3")
    fun getFlights(): Call<FlightsResponse>

}
