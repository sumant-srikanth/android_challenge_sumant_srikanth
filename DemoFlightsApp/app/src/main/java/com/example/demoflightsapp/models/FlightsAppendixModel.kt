package com.example.demoflightsapp.models

class FlightsAppendixModel(
    var airlines: HashMap<String, String>, var airports: HashMap<String, String>,
    var providers: HashMap<Int, String>
)