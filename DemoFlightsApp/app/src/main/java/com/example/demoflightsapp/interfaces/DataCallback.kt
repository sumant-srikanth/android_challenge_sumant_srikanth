package com.example.demoflightsapp.interfaces

import com.example.demoflightsapp.models.FlightsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

interface DataCallback<T> {
    fun onSuccess(response: T)
    fun onFailure(t: Throwable, isCancelled: Boolean)

    companion object {
        fun <T> convertToRetrofit(callback: DataCallback<T>): Callback<T> {
            return object : Callback<T> {
                override fun onFailure(call: Call<T>, t: Throwable) {
                    callback.onFailure(t, call.isCanceled)
                }

                override fun onResponse(call: Call<T>, response: Response<T>) {
                    try {
                        callback.onSuccess(response.body()!!)
                    } catch (t: Throwable) {
                        callback.onFailure(t, false)
                    }
                }
            }
        }
    }
}